package com.example.tmamalakis1.powerweatherandr;

import android.content.Context;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    final String DEGREE  = "\u00b0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void getWeather(View v)
    {

        // Get the text from the input field

        EditText location = (EditText) findViewById(R.id.editText);
        String zip = location.getText().toString();
        new GetWeatherInBackground().execute(zip);


    }
    public void getDayTwo(View v)
    {

        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.editText);
        String zip = location.getText().toString();
        new GetDayTwoInBackground().execute(zip);

    }
    public void getDayThree(View v)
    {

        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.editText);
        String zip = location.getText().toString();
        new GetDayThreeInBackground().execute(zip);

    }
    public void getDayFour(View v)
    {

        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.editText);
        String zip = location.getText().toString();
        new GetDayFourInBackground().execute(zip);

    }
    public void getDayFive(View v)
    {

        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.editText);
        String zip = location.getText().toString();
        new GetDayFiveInBackground().execute(zip);

    }

    public void getRadar(View v)
    {

        // Get the text from the input field
        EditText location = (EditText) findViewById(R.id.editText);
        String zip = location.getText().toString();
        new GetRadarInBackground().execute(zip);

    }

    private class GetRadarInBackground extends AsyncTask<String, Void, PowerWeather>
    {
        @Override
        protected PowerWeather doInBackground(String... locations)
        {
            // Fetch the weather data
            PowerWeather w = new PowerWeather(locations[0]);
            return  w;
        }

        @Override
        protected void onPostExecute(PowerWeather w)
        {
            EditText location = (EditText) findViewById(R.id.editText);
            String z = location.getText().toString();
            WebView radar = (WebView) findViewById(R.id.Radar);
            radar.loadUrl("http://api.wunderground.com/api/ca51a08ea2deeeca/radar/satellite/q/"+ z +".png?radius=25&width=300&height=300");

        }
    }


    private class GetWeatherInBackground extends AsyncTask<String, Void, PowerWeather>
    {
        @Override
        protected PowerWeather doInBackground(String... locations)
        {
            // Fetch the weather data
            PowerWeather w = new PowerWeather(locations[0]);
            return  w;
        }

        @Override
        protected void onPostExecute(PowerWeather w)
        {
            // Update the data on the screen
            TextView temp = (TextView) findViewById(R.id.textView8);
            temp.setText("" + w.getTemp() + DEGREE + "F");

            TextView city = (TextView) findViewById(R.id.textView4);
            city.setText("" + w.getCity());

            TextView high = (TextView) findViewById(R.id.textView7);
            high.setText("" + w.getHigh() + DEGREE + "F");

            TextView low = (TextView) findViewById(R.id.textView6);
            low.setText("" + w.getLow() + DEGREE + "F");

            TextView weather = (TextView) findViewById(R.id.textView5);
            weather.setText("" + w.getWeather());

            TextView date = (TextView) findViewById(R.id.textView10);
            date.setText("" + w.getMonthDOne() + "/" + w.getDayDOne() + "/" + w.getYearDOne());

            TextView day = (TextView) findViewById((R.id.textView9));
            day.setText("" + w.getDayOfWeekOne());

            ImageView img = (ImageView) findViewById(R.id.imageView);
            Context context = img.getContext();
            img.setImageResource(context.getResources().getIdentifier("drawable/" + w.getIcon(), null, context.getPackageName()));

            


        }
    }
    private class GetDayTwoInBackground extends AsyncTask<String, Void, PowerWeather>
    {
        @Override
        protected PowerWeather doInBackground(String... locations)
        {
            // Fetch the weather data
            PowerWeather w = new PowerWeather(locations[0]);
            return  w;
        }

        @Override
        protected void onPostExecute(PowerWeather w)
        {
            // Update the data on the screen
            TextView temp = (TextView) findViewById(R.id.textView8);
            temp.setText("" + w.getTemp() + DEGREE + "F");

            TextView city = (TextView) findViewById(R.id.textView4);
            city.setText("" + w.getCity());

            TextView high = (TextView) findViewById(R.id.high);
            high.setText("" + w.getDTwoHigh() + DEGREE + "F");

            TextView low = (TextView) findViewById(R.id.low);
            low.setText("" + w.getDTwoLow() + DEGREE + "F");

            TextView weather = (TextView) findViewById(R.id.condition);
            weather.setText("" + w.getDTwoWeather());

            TextView date = (TextView) findViewById(R.id.date);
            date.setText("" + w.getDTWoDate());

            TextView day = (TextView) findViewById((R.id.dayofweek));
            day.setText("" + w.getDTWoDayofWeek());

            ImageView img = (ImageView) findViewById(R.id.imageView2);
            Context context = img.getContext();
            img.setImageResource(context.getResources().getIdentifier("drawable/" + w.getDTWoIcon(), null, context.getPackageName()));


        }
    }
    private class GetDayThreeInBackground extends AsyncTask<String, Void, PowerWeather>
    {
        @Override
        protected PowerWeather doInBackground(String... locations)
        {
            // Fetch the weather data
            PowerWeather w = new PowerWeather(locations[0]);
            return  w;
        }

        @Override
        protected void onPostExecute(PowerWeather w)
        {
            // Update the data on the screen
            TextView temp = (TextView) findViewById(R.id.textView8);
            temp.setText("" + w.getTemp() + DEGREE + "F");

            TextView city = (TextView) findViewById(R.id.textView4);
            city.setText("" + w.getCity());

            TextView high = (TextView) findViewById(R.id.high);
            high.setText("" + w.getDThreeHigh() + DEGREE + "F");

            TextView low = (TextView) findViewById(R.id.low);
            low.setText("" + w.getDThreeLow() + DEGREE + "F");

            TextView weather = (TextView) findViewById(R.id.condition);
            weather.setText("" + w.getDThreeWeather());

            TextView date = (TextView) findViewById(R.id.date);
            date.setText("" + w.getDThreeDate());

            TextView day = (TextView) findViewById((R.id.dayofweek));
            day.setText("" + w.getDThreeDayofWeek());

            ImageView img = (ImageView) findViewById(R.id.imageView2);
            Context context = img.getContext();
            img.setImageResource(context.getResources().getIdentifier("drawable/" + w.getDThreeIcon(), null, context.getPackageName()));

        }
    }
    private class GetDayFourInBackground extends AsyncTask<String, Void, PowerWeather>
    {
        @Override
        protected PowerWeather doInBackground(String... locations)
        {
            // Fetch the weather data
            PowerWeather w = new PowerWeather(locations[0]);
            return  w;
        }

        @Override
        protected void onPostExecute(PowerWeather w)
        {
            // Update the data on the screen
            TextView temp = (TextView) findViewById(R.id.textView8);
            temp.setText("" + w.getTemp() + DEGREE + "F");

            TextView city = (TextView) findViewById(R.id.textView4);
            city.setText("" + w.getCity());

            TextView high = (TextView) findViewById(R.id.high);
            high.setText("" + w.getDFourHigh() + DEGREE + "F");

            TextView low = (TextView) findViewById(R.id.low);
            low.setText("" + w.getDFourLow() + DEGREE + "F");

            TextView weather = (TextView) findViewById(R.id.condition);
            weather.setText("" + w.getDFourWeather());

            TextView date = (TextView) findViewById(R.id.date);
            date.setText("" + w.getDFourDate());

            TextView day = (TextView) findViewById((R.id.dayofweek));
            day.setText("" + w.getDFourDayofWeek());

            ImageView img = (ImageView) findViewById(R.id.imageView2);
            Context context = img.getContext();
            img.setImageResource(context.getResources().getIdentifier("drawable/" + w.getDFourIcon(), null, context.getPackageName()));


        }
    }
    private class GetDayFiveInBackground extends AsyncTask<String, Void, PowerWeather>
    {
        @Override
        protected PowerWeather doInBackground(String... locations)
        {
            // Fetch the weather data
            PowerWeather w = new PowerWeather(locations[0]);
            return  w;
        }

        @Override
        protected void onPostExecute(PowerWeather w)
        {
            // Update the data on the screen
            TextView temp = (TextView) findViewById(R.id.textView8);
            temp.setText("" + w.getTemp() + DEGREE + "F");

            TextView city = (TextView) findViewById(R.id.textView4);
            city.setText("" + w.getCity());

            TextView high = (TextView) findViewById(R.id.high);
            high.setText("" + w.getDFiveHigh() + DEGREE + "F");

            TextView low = (TextView) findViewById(R.id.low);
            low.setText("" + w.getDFiveLow() + DEGREE + "F");

            TextView weather = (TextView) findViewById(R.id.condition);
            weather.setText("" + w.getDFiveWeather());

            TextView date = (TextView) findViewById(R.id.date);
            date.setText("" + w.getDFiveDate());

            TextView day = (TextView) findViewById((R.id.dayofweek));
            day.setText("" + w.getDFiveDayofWeek());

            ImageView img = (ImageView) findViewById(R.id.imageView2);
            Context context = img.getContext();
            img.setImageResource(context.getResources().getIdentifier("drawable/" + w.getDFiveIcon(), null, context.getPackageName()));




        }
    }
}
