package com.example.tmamalakis1.powerweatherandr; /**
 * Created by faval_000 on 11/16/2016.
 * Edited by Joseph on 11/18/2016.
 */

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class PowerWeather {
    private final String TOKEN = "c5cabc578dd08bdaa49da8e37698667ff5c0f41e";
    private JsonElement jse = null;

    public PowerWeather(String zip) {


        try {

            // Construct URL object out of encodedURL
            URL WuURL = new URL("https://api.wunderground.com/api/ca51a08ea2deeeca/conditions/forecast10day/radar/q/" + zip + ".json");

            // Make InputStream from URL
            InputStream is = WuURL.openStream();

            // Attach to BufferedReader
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            // Attach to JSON parser
            jse = new JsonParser().parse(br);

            is.close();
            br.close();


        } catch (java.io.UnsupportedEncodingException uee) {
            uee.printStackTrace();
        } catch (java.net.MalformedURLException mue)

        {
            mue.printStackTrace();
        } catch (java.io.IOException ioe)

        {
            ioe.printStackTrace();
        }


    }


    public String getCity() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
    }

    public String getTemp() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsString();
    }

    public String getWeather() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    public String getIcon() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString();
    }

    public String getHumidity() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("relative_humidity").getAsString();
    }

    public String getHigh() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getLow() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getRadar() {
        return jse.getAsJsonObject().get("radar").getAsJsonObject().get("image url").getAsString();
    }

    public String getDayDOne()//get day for current day
    {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString();
    }

    public String getDayOfWeekOne()
    {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
    }

    public String getMonthDOne()//get month for current day
    {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString();
    }

    public String getYearDOne()//get year for current day
    {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();
    }

    public String getUpdateTime() {
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("observation_time").getAsString();
    }


    public String getDTwoHigh() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDThreeHigh() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDFourHigh() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDFiveHigh() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(5).getAsJsonObject().get("high").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDTwoLow() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDThreeLow() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDFourLow() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDFiveLow() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(5).getAsJsonObject().get("low").getAsJsonObject().get("fahrenheit").getAsString();
    }

    public String getDTwoWeather() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1).getAsJsonObject().get("conditions").getAsString();
    }

    public String getDThreeWeather() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("conditions").getAsString();
    }

    public String getDFourWeather() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("conditions").getAsString();
    }

    public String getDFiveWeather() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("conditions").getAsString();
    }

    public String getDTwoHumidity() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("avehumidity").getAsString();
    }

    public String getDThreeHumidity() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("avehumidity").getAsString();
    }

    public String getDFourHumidity() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("avehumidity").getAsString();
    }

    public String getDFiveHumidity() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(5).getAsJsonObject().get("avehumidity").getAsString();
    }

    public String getDTWoDate() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1).getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1).getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();
    }

    public String getDThreeDate() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();
    }

    public String getDFourDate() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();
    }

    public String getDFiveDate() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("date").getAsJsonObject().get("month").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("date").getAsJsonObject().get("day").getAsString() + "/"
                + jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("date").getAsJsonObject().get("year").getAsString();
    }
    public String getDOneDayofWeek() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(0).getAsJsonObject().get("date").getAsJsonObject().get("weekday").getAsString();
    }
    public String getDTWoDayofWeek() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1).getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString();
    }
    public String getDThreeDayofWeek() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString();
    }

    public String getDFourDayofWeek() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString();
    }

    public String getDFiveDayofWeek() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("date").getAsJsonObject().get("weekday_short").getAsString();
    }
    public String getDTWoIcon() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(1).getAsJsonObject().get("icon").getAsString();
    }

    public String getDThreeIcon() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(2).getAsJsonObject().get("icon").getAsString();
    }

    public String getDFourIcon() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(3).getAsJsonObject().get("icon").getAsString();
    }

    public String getDFiveIcon() {
        return jse.getAsJsonObject().get("forecast").getAsJsonObject().get("simpleforecast").getAsJsonObject().get("forecastday").getAsJsonArray().get(4).getAsJsonObject().get("icon").getAsString();
    }


    public static void main(String[] args) {
        PowerWeather b = new PowerWeather("95681");
        String city = b.getCity();
        String temp = b.getTemp();
        String weather = b.getWeather();
        String humidity = b.getHumidity();
        String high = b.getHigh();
        String low = b.getLow();
        String icon = b.getIcon();
        String radar = b.getRadar();
        String day = b.getDayDOne();
        String month = b.getMonthDOne();
        String year = b.getYearDOne();
        String DTwoHigh = b.getDTwoHigh();
        String DThreeHigh = b.getDThreeHigh();
        String DFourHigh = b.getDFourHigh();
        String DFiveHigh = b.getDFiveHigh();
        String DTwoLow = b.getDTwoLow();
        String DThreeLow = b.getDThreeLow();
        String DFourLow = b.getDFourLow();
        String DFiveLow = b.getDFiveLow();
        String DTwoFaren = b.getDTwoWeather();
        String DThreeFaren = b.getDThreeWeather();
        String DFourFaren = b.getDFourWeather();
        String DFiveFaren = b.getDFiveWeather();
        String DTwoDate = b.getDTWoDate();
        String DThreeDate = b.getDThreeDate();
        String DFourDate = b.getDFourDate();
        String DFiveDate = b.getDFiveDate();
        System.out.println(city + " " + temp + " " + weather + " " + icon);
        System.out.println(high + " " + low + " " + humidity + " " + day + " " + year + " " + month);
        System.out.println(radar);
        System.out.println(DTwoHigh + " " + DThreeHigh + " " + DFourHigh + " " + DFiveHigh);
        System.out.println(DTwoFaren + " " + DThreeFaren + " " + DFourFaren + " " + DFiveFaren);
        System.out.println(DTwoDate + " " + DThreeDate + " " + DFourDate + " " + DFiveDate);
    }
}
